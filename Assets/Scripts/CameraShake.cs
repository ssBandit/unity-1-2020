﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    //Singleton
    public static CameraShake self;

    Vector3 startPosition;

    void Start()
    {
        self = this;
    }

    public IEnumerator Shake(float time, float force)
    {
        startPosition = transform.position;

        float startTime = Time.time;
        while (Time.time - startTime <= time)
        {
            float randX = Random.Range(-1.0f, 1.0f) * force;
            float randY = Random.Range(-1.0f, 1.0f) * force;

            transform.position = new Vector3(transform.position.x + randX, transform.position.y + randY, transform.position.z);
            yield return null;
        }

        transform.position = startPosition;
    }
}
