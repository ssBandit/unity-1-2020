﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 5;
    Rigidbody2D rb;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        //Вызвать функцию Kill() через 5 секунд
        Invoke("Kill", 5);
    }
    
    void Update()
    {
        //Двигатся в свое направление вправо умноженное на скорость
        rb.velocity = transform.right * speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Если столкнулся с чем-то то самоуничтожится
        Kill();
    }

    void Kill()
    {
        //Самоуничтожение
        Destroy(gameObject);
    }
}
