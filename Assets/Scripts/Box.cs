﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    //Количество выстрелов нужное для уничтожения
    public int hp = 3;
    //Объект для системы частиц
    public GameObject particles;

    [Range(0, 2)]public float shakeTime = 1;
    [Range(0, 1)]public float shakeForce = 0.2f;

    public AudioClip explosionSound;

    Animator anim;

    AudioSource sound;

    private void Start()
    {
        anim = GetComponent<Animator>();
        sound = GetComponent<AudioSource>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        //Если столкнулся с пулей то отнять у себя жизней
        if(collision.gameObject.CompareTag("Bullet"))
        {
            hp--;
            anim.SetTrigger("Hit");
            sound.Play();
        }
        //Если жизней нет то создать частички и самоуничтожится
        if(hp <= 0)
        {
            GameObject s = new GameObject("explosion sound", typeof(AudioSource));
            s.transform.position = this.transform.position;
            s.GetComponent<AudioSource>().PlayOneShot(explosionSound);
            Destroy(s, explosionSound.length + 0.1f);

            CameraShake.self.StartCoroutine(CameraShake.self.Shake(shakeTime, shakeForce));

            Instantiate(particles, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

}
