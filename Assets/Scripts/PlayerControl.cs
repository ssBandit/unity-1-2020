﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    //Скорость игрока и сила прыжка
    public float speed = 2;
    public float jumpForce = 50;
    //Оюъект для пули
    public GameObject bullet;
    public float recoil = 5;

    public AudioClip jumpSound;
    public AudioClip shootSound;

    Rigidbody2D rb;
    Animator anim;
    AudioSource audioSource;
    
    void Start()
    {
        //Получить Rigidbody2D из самого-себя
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        //Получить горизонтальное направление (от -1 до 1) и умножить на скорость
        float move = Input.GetAxis("Horizontal") * speed;

        //Задать скорость на направление по Х и оставить Y своим
        rb.velocity = new Vector2(move, rb.velocity.y);

        //Если нажали на кнопку прыгнуть то добавить силы вверх
        if(Input.GetButtonDown("Jump"))
        {
            rb.AddForce(Vector2.up * jumpForce);
            anim.SetTrigger("Jump");
            audioSource.PlayOneShot(jumpSound);
        }

        if(Input.GetButtonDown("Fire1"))
        {
            //Вычеслить угол между игроком и точкой куда кликнули
            Vector2 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 direction = worldPos - (Vector2)(transform.position + Vector3.up * 0.5f);
            float degreess = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            //Создать пулю на позиции игрока с вращением вокруг Z оси на столько на сколько вычеслили
            Instantiate(bullet, transform.position + Vector3.up * 0.5f, Quaternion.AngleAxis(degreess, Vector3.forward));

            rb.AddForce(-direction.normalized * recoil, ForceMode2D.Impulse);

            audioSource.pitch = Random.Range(0.7f, 1.3f);
            audioSource.PlayOneShot(shootSound);
            
            CameraShake.self.StartCoroutine(CameraShake.self.Shake(0.1f, 0.05f));
        }

        //Если нажали Escape то закрыть игру
        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
